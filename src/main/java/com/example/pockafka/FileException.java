package com.example.pockafka;

/**
 * @author Sompong Thongmee
 * @create 13/08/2022 11:46
 * @project poc-kafka
 */
public class FileException extends RuntimeException {
  public FileException(String message) {
    super(message);
  }
}
