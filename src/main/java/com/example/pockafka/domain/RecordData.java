package com.example.pockafka.domain;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Sompong Thongmee
 * @create 06/08/2022 12:28
 * @project poc-kafka
 */
@Getter
@Setter
public class RecordData {
  private int maxLength;
  private List<String> body;
}
