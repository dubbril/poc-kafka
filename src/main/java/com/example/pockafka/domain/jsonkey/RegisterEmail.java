package com.example.pockafka.domain.jsonkey;

import static com.example.pockafka.utils.Utils.countBytes;

import java.util.stream.Stream;

/**
 * @author Sompong Thongmee
 * @create 10/08/2022 01:53
 * @project poc-kafka
 */
@SuppressWarnings("java:S115")
public enum RegisterEmail {
  register, appNo, cifNo, accountNo, email;

  //+8 is count of symbol { " " : " ", } for  generate format key of jso
  public static int byteOfKey() {
    return Stream.of(RegisterEmail.values()).map(str -> countBytes.applyAsInt(str.name()))
        .map(i -> i + 8)
        .reduce(0, Integer::sum);
  }
}
