package com.example.pockafka.controller;

import com.example.pockafka.service.ProcessDataService;
import com.example.pockafka.service.ReadWriteFileService;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sompong Thongmee
 * @create 24/07/2022 11:42
 * @project poc-kafka
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "api/v1/kafka")
public class PocController {

  private final ProcessDataService processDataService;
  private final ReadWriteFileService readWriteFileService;

  @GetMapping(path = "/send-msg", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> sendMessage() {
    JSONObject jsonObject = processDataService.sendDataToKafka();
    return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
  }

  @GetMapping(path = "/make-data")
  public ResponseEntity<Void> makeRawData(@RequestParam int recordSize) {
    readWriteFileService.makeFile(recordSize);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @GetMapping(path = "/read-file", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> makeRawData(@RequestParam String fileName) {
    readWriteFileService.readFromFile(fileName);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
