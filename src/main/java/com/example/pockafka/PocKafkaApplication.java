package com.example.pockafka;

import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@SpringBootApplication
@PropertySource(value = "file:/home/jboss/config/poc-kafka.properties", ignoreResourceNotFound =
    true)
public class PocKafkaApplication {

  public static void main(String[] args) {
    SpringApplication.run(PocKafkaApplication.class, args);
  }

  @Bean
  public PropertiesFactoryBean propertiesFileMapping() {
    PropertiesFactoryBean factoryBean = new PropertiesFactoryBean();
    factoryBean.setFileEncoding("UTF-8");
    factoryBean.setLocation(new ClassPathResource("application.properties"));
    return factoryBean;
  }
}
