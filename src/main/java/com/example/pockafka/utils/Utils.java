package com.example.pockafka.utils;


import java.nio.charset.StandardCharsets;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;
import lombok.experimental.UtilityClass;

/**
 * @author Sompong Thongmee
 * @create 07/08/2022 21:55
 * @project poc-kafka
 */
@UtilityClass
public class Utils {

  public static final ToIntFunction<String> countBytes =
      str -> str.getBytes(StandardCharsets.UTF_8).length;

  public static final ToIntBiFunction<String, Integer> getMaxLength =
      (x, y) -> Math.max(countBytes.applyAsInt(x), y);


}
