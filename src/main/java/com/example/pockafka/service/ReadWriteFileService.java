package com.example.pockafka.service;


import static com.example.pockafka.utils.Utils.getMaxLength;

import com.example.pockafka.FileException;
import com.example.pockafka.domain.RecordData;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Sompong Thongmee
 * @create 25/07/2022 10:54
 * @project poc-kafka
 */
@Service
public class ReadWriteFileService {

  @Value("${location-path-file}")
  private String pathFile;

  @Value("${location-example-file-path}")
  private String exampleFilePath;

  @SneakyThrows
  public void makeFile(int recordSize) {
    String baseValueMockFile = readExampleFile();
    var rawDataList = IntStream.rangeClosed(1, recordSize).mapToObj(
        i -> baseValueMockFile).collect(Collectors.toList());
    Files.write(Paths.get(pathFile), rawDataList);
  }

  public RecordData readFromFile(String fileName) {
    try (BufferedReader reader = new BufferedReader(new FileReader(pathFile))) {
      RecordData recordData = new RecordData();
      var bodyList = new ArrayList<String>();
      String dateAndRoundFromFileName = fileName.replaceAll("\\D", "");
      String dateFileName = dateAndRoundFromFileName.substring(0, 8);
      String roundFileName = dateAndRoundFromFileName.substring(8);

      reader.lines().forEach(line -> {
        switch (line.charAt(0)) {
          case 'H':
            String[] splitRecordHeader = line.split("\\|");
            if (!dateFileName.equals(splitRecordHeader[1])) {
              throw new FileException("Invalid Header Date In Header Not Match");
            } else if (!roundFileName.equals(splitRecordHeader[4])) {
              throw new FileException("Invalid Header Round In Header Not Match");
            }
            break;
          case 'D':
            bodyList.add(line);
            recordData.setMaxLength(getMaxLength.applyAsInt(line, recordData.getMaxLength()) - 5);
            break;
          case 'T':
            String[] splitRecordTail = line.split("\\|");
            if (!dateFileName.equals(splitRecordTail[1])) {
              throw new FileException("Invalid Tail Date In Tail Not Match");
            } else if (!roundFileName.equals(splitRecordTail[3])) {
              throw new FileException("Invalid Tail Round In Tail Not Match");
            } else if (Integer.parseInt(splitRecordTail[5]) != bodyList.size()) {
              throw new FileException("Invalid Tail Total Record In Tail Not Match");
            }
            break;
          default:
            throw new FileException("Invalid Data File Each Line Would Start With H,D,T Only");
        }
      });
      recordData.setBody(bodyList);
      return recordData;

    } catch (IOException e) {
      throw new FileException(e.getMessage());
    }
  }

  private String readExampleFile() {
    File exampleFile = new File(exampleFilePath);
    try (BufferedReader reader = new BufferedReader(new FileReader(exampleFile))) {
      return reader.lines().collect(Collectors.joining(System.lineSeparator()));
    } catch (IOException e) {
      throw new FileException(e.getMessage());
    }
  }
}
