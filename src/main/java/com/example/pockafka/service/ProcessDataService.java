package com.example.pockafka.service;

import com.example.pockafka.domain.RecordData;
import com.example.pockafka.domain.jsonkey.Instrument;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Sompong Thongmee
 * @create 24/07/2022 13:16
 * @project poc-kafka
 */
@Service
@RequiredArgsConstructor
public class ProcessDataService {

  private final TopicProducerService topicProducerService;
  private final ReadWriteFileService readWriteFileService;
  private final BiFunction<Instant, Instant, Long> exeTimes =
      (start, end) -> Duration.between(start, end).getSeconds();
  private final JSONObject resultExecution = new JSONObject();

  @Value("${spring.kafka.producer.properties.max.request.size}")
  private int limitBytes;

  public JSONObject sendDataToKafka() {
    List<String> messageJsonList = createMessageJsonList();
    Instant start = Instant.now();
    messageJsonList.forEach(topicProducerService::send);
    Instant end = Instant.now();
    resultExecution.put("Send Kafka Times", exeTimes.apply(start, end) + " seconds");
    return resultExecution;
  }

  //+1 is keep 1% for include generate syntax of json object
  private int findMaxLengthPerRecord(int maxBytes, int keyBytes) {
    if (keyBytes > maxBytes) {
      int ratioKeys = (maxBytes * 100 / keyBytes) + 1;
      return (limitBytes - (limitBytes * ratioKeys / 100)) / keyBytes;
    }
    int ratioKeys = (keyBytes * 100 / maxBytes) + 1;
    return (limitBytes - (limitBytes * ratioKeys / 100)) / maxBytes;
  }

  private List<String> createMessageJsonList() {
    RecordData recordData = readRawData();
    List<String> rawDataList = recordData.getBody();
    Instant start = Instant.now();
    List<String> resultJsonList = new ArrayList<>();
    int maxLength = findMaxLengthPerRecord(recordData.getMaxLength(), Instrument.byteOfKey());
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("textId", "BEE126CC-A376-4C5C-B58D-95A48CAF5A65");

    JSONArray jsonArray = new JSONArray();
    for (int i = 0; i < rawDataList.size(); i++) {
      if (i % maxLength == 0 && i > 0) {
        jsonObject.put(Instrument.datas.name(), jsonArray);
        resultJsonList.add(jsonObject.toString());
        jsonArray = new JSONArray();
      }
      jsonArray.put(new JSONObject().put(Instrument.values.name(), rawDataList.get(i)));
    }

    // Check Last Record
    if (jsonArray.length() > 0) {
      jsonObject.put(Instrument.datas.name(), jsonArray);
      resultJsonList.add(jsonObject.toString());
    }
    Instant end = Instant.now();
    resultExecution.put("Process Json Times", exeTimes.apply(start, end) + " Seconds");
    return resultJsonList;
  }

  private RecordData readRawData() {
    Instant start = Instant.now();
    RecordData recordData = readWriteFileService.readFromFile("TCRB-Installment-20220816001.txt");
    Instant end = Instant.now();
    resultExecution.put("Read File Times", exeTimes.apply(start, end) + " Seconds");
    return recordData;
  }
}
