package com.example.pockafka.service;

/**
 * @author Sompong Thongmee
 * @create 24/07/2022 11:52
 * @project poc-kafka
 */

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TopicProducerService {

  private final KafkaTemplate<String, String> kafkaTemplate;
  @Value("${topic.name.producer}")
  private String topicName;

  public void send(String message) {
    kafkaTemplate.send(topicName, UUID.randomUUID().toString(), message);
  }
}
